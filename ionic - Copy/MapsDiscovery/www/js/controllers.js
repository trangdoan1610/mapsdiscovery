angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('AddEventCtrl', function($scope, $ionicModal, $ionicPopup) {

  var markers = [];
  $scope.eventData = {};
  $scope.eventData.Date = new Date();
  $scope.eventData.Time = new Date();
  $scope.eventData.EventName = "";
  $scope.eventData.Organizer = "";
  $scope.eventData.img = "img/ionic.png";



  $scope.showMap = function() {
    var lat = 21.0277644;
    var lng = 105.83415979999995;
    if (markers[0] != null) {
      lat = markers[0].getPosition().lat();
      lng = markers[0].getPosition().lng();
    }
    var myLatlng = new google.maps.LatLng(lat, lng);

    var mapOptions = {
      center: myLatlng,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map"), mapOptions);
    if (markers[0] != null) {
      markers.push(new google.maps.Marker({
        map: map,
        position: markers[0].getPosition(),
      }));
    }
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });
    searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();
      if (places.length == 0) {
        return;
      }
      markers.forEach(function(marker) {
        marker.setMap(null);
      });
      markers = [];
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
        var icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };
        // Create a marker for each place
        markers.push(new google.maps.Marker({
          map: map,
          icon: icon,
          title: place.name,
          position: place.geometry.location,
          draggable: true,
          animation: google.maps.Animation.DROP
        })
      );
      if (place.geometry.viewport) {
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });
  // $scope.map = map;
};

$ionicModal.fromTemplateUrl('templates/maps.html', function($ionicModal) {
  $scope.modal = $ionicModal;
}, {
  scope: $scope,
  animation: 'slide-in-up'
});


$scope.loadLocation = function() {
  $scope.modal.show();
  $scope.showMap();
};

$scope.getLocation = function() {
  pos = markers[0].getPosition();
  var name = markers[0].getTitle();
  map.setCenter(pos);
  $scope.eventData.LocationTitle = markers[0].getTitle();
  $scope.eventData.LocationLat = pos.lat();
  $scope.eventData.LocationLng = pos.lng();
  console.log("get location ");
  $scope.modal.hide();
};

$scope.closeMaps = function() {
  $ionicModal.hide();
};

$scope.doAddEvent = function() {
  connectDB();
  insertEvent($scope.eventData.EventName, $scope.eventData.LocationTitle, $scope.eventData.LocationLat, $scope.eventData.LocationLng, $scope.eventData.Date, $scope.eventData.Time, $scope.eventData.Organizer, $scope.eventData.img);
  console.log('Doing Add Event' + $scope.eventData + "Name Event " + $scope.eventData.EventName);
};
})



.directive('map', function() {
  return {
    restrict: 'E',
    scope: {
      onCreate: '&'
    },
    link: function($scope, $element, $attr) {
      function initialize() {
        var mapOptions = {
          center: new google.maps.LatLng(21.0277644, 105.83415979999995),
          zoom: 13,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map($element[0], mapOptions);

        $scope.onCreate({ map: map });

        // Stop the side bar from dragging when mousedown/tapdown on the map
        google.maps.event.addDomListener($element[0], 'mousedown', function(e) {
          e.preventDefault();
          return false;
        });
      }

      if (document.readyState === "complete") {
        initialize();
      } else {
        google.maps.event.addDomListener(window, 'load', initialize);
      }
    }
  }
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
});
