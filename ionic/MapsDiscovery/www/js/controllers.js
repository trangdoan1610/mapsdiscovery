angular.module('starter.controllers', [])

    .controller('AppCtrl', function($scope, $ionicModal, $timeout) {

        // With the new view caching in Ionic, Controllers are only called
        // when they are recreated or on app start, instead of every page change.
        // To listen for when this page is active (for example, to refresh data),
        // listen for the $ionicView.enter event:
        //$scope.$on('$ionicView.enter', function(e) {
        //});

        // Form data for the login modal
        $scope.loginData = {};

        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('templates/login.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });

        // Triggered in the login modal to close it
        $scope.closeLogin = function() {
            $scope.modal.hide();
        };

        // Open the login modal
        $scope.login = function() {
            $scope.modal.show();
        };

        // Perform the login action when the user submits the login form
        $scope.doLogin = function() {
            console.log('Doing login', $scope.loginData);

            // Simulate a login delay. Remove this and replace with your login
            // code if using a login system
            $timeout(function() {
                $scope.closeLogin();
            }, 1000);
        };
    })

    .controller('AddEventCtrl', function($scope, $ionicModal, $ionicPopup, $timeout) {

        $scope.eventData = {};
        $scope.eventData.Date = new Date();
        var hour = new Date().getHours();
        var second = new Date().getSeconds();
        $scope.eventData.Time = hour + " : " + second;
        $scope.eventData.Organizer = "Organizer";
        $scope.eventData.img = "img/ionic.png";

        $ionicModal.fromTemplateUrl('templates/maps.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });

        $scope.closeMaps = function() {
            $scope.modal.hide();
        };

        $scope.loadLocation = function() {
            $scope.modal.show();
        };

        $scope.closeModal = function() {
            $scope.modal.hide();
        };

        $scope.doAddEvent = function() {
            connectDB();
            insertEvent($scope.EventName, $scope.LocationTitle, $scope.LocationLat, $scope.LocationLng, $scope.Date, $scope.Time, $scope.Organizer, $scope.eventData.img);
            console.log('Doing Add Event' + $scope.eventData + "Name Event " + $scope.eventData.EventName);
        };
    })

    // .controller('PlaylistsCtrl', function($scope) {
    //     $scope.playlists = [
    //         { title: 'Reggae', id: 1 },
    //         { title: 'Chill', id: 2 },
    //         { title: 'Dubstep', id: 3 },
    //         { title: 'Indie', id: 4 },
    //         { title: 'Rap', id: 5 },
    //         { title: 'Cowbell', id: 6 }
    //     ];
    // })
    .controller('PlaylistsCtrl', function($scope) {
        $scope.playlists = [
            { title: 'Reggae', id: 1 },
            { title: 'Chill', id: 2 },
            { title: 'Dubstep', id: 3 },
            { title: 'Indie', id: 4 },
            { title: 'Rap', id: 5 },
            { title: 'Cowbell', id: 6 }
        ];
        var lstEvent = [];
    })

    .controller('PlaylistCtrl', function($scope, $stateParams) {
    })

    .controller('MapController', function($scope, $ionicLoading) {

        google.maps.event.addDomListener(window, 'load', function() {
            var myLatlng = new google.maps.LatLng(37.3000, -120.4833);

            var mapOptions = {
                center: myLatlng,
                zoom: 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(document.getElementById("map"), mapOptions);

            navigator.geolocation.getCurrentPosition(function(pos) {
                map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
                var myLocation = new google.maps.Marker({
                    position: new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude),
                    map: map,
                    title: "My Location"
                });
            });

            $scope.map = map;
        });
    });

var db = null;

function connectDB() {
    db = window.openDatabase("MadDiscovery", 1.0, "Mad Discovery", 2000000);
    var url = window.location.href;
    if (db != null) {
        createTable();
        console.log("CONNECT DATABASE OK");
    }
}

function createTable() {
    db.transaction(function(tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS Event(ID INTEGER PRIMARY KEY AUTOINCREMENT, EventName, LocationTitle, LocationLat, LocationLng, Date, Time, Organizer, urlImage);");
        tx.executeSql("CREATE TABLE IF NOT EXISTS Report(ID INTEGER PRIMARY KEY AUTOINCREMENT, EventID, Discription);");
        // tx.executeSql("CREATE TABLE IF NOT EXISTS LocationEvent(ID INTEGER PRIMARY KEY AUTOINCREMENT, EventID, LocationTitle, LocationLat, LocationLng);");
        tx.executeSql("CREATE TABLE IF NOT EXISTS LocationLive(ID INTEGER PRIMARY KEY AUTOINCREMENT, LocationTitle, LocationLat, LocationLng);");
        tx.executeSql("CREATE TABLE IF NOT EXISTS Image(ID INTEGER PRIMARY KEY AUTOINCREMENT, ImageURL, ReportID);");
    }, function(err) {
        console.log("CREATE TABLE ERROR " + err.message);
    }, function() {
        console.log("CREATE TABLE OK");
    });
}
function dropTable() {
    db.transaction(function(tx) {
        tx.executeSql("DROP TABLE IF EXISTS Event;");
        tx.executeSql("DROP TABLE IF EXISTS Report;");
        // tx.executeSql("CREATE TABLE IF NOT EXISTS LocationEvent(ID INTEGER PRIMARY KEY AUTOINCREMENT, EventID, LocationTitle, LocationLat, LocationLng);");
        tx.executeSql("DROP TABLE IF EXISTS LocationLive;");
        tx.executeSql("DROP TABLE IF EXISTS Image;");
    }, function(err) {
        console.log("DROP TABLE ERROR " + err.message);
    }, function() {
        console.log("DROP TABLE OK");
    });
}

function insertEvent(Name, LocationTitle, LocationLat, LocationLng, Date, Time, Organizer, imgUrl) {
    db.transaction(function(tx) {
        console.log("Insert Start");
        tx.executeSql("INSERT INTO Event(EventName, LocationTitle, LocationLat, LocationLng, Date, Time, Organizer,urlImage) VALUES(?, ?, ?, ?, ?, ?, ?, ?);",
            [Name, LocationTitle, LocationLat, LocationLng, Date, Time, Organizer]);
        console.log("INSERT EVENT : lạks " + tx);
        // window.location = "index.html";
    }, function(err) {
        console.log("INSERT EVENT ERROR " + err.message);
    }, function() {
        console.log("INSERT EVENT Ok ");
    });
}

function insertReport(EventID, Discription) {
    db.transaction(function(tx) {
        tx.executeSql("INSERT INTO Report(EventID,Discription) VALUES(?,?);", [EventID, Discription]);
    }, function(err) {
        console.log("INSERT REPORT ERROR " + err.message);
    }, function() {
        console.log("INSERT REPORT OK");
    });
}

// Create new location you live now
function insertLocationLive(LocationTitle, LocationLat, LocationLng) {
    db.transaction(function(tx) {
        tx.executeSql("INSERT INTO LocationEvent(LocationTitle, LocationLat, LocationLng) VALUES(?,?,?);", [LocationTitle, LocationLat, LocationLng]);
    }, function(err) {
        console.log("INSERT LOCATION LIVE ERROR " + err.message);
    }, function() {
        console.log("INSERT LOCATION LIVE OK");
    });
}


function deleteEvent(eventID) {
    db.transaction(function(tx) {
        tx.executeSql("DELETE FROM Event WHERE ID = ?", [eventID]);
        tx.executeSql("DELETE FROM Report WHERE EventID = ?", [eventID]);
        tx.executeSql("DELETE FROM LocationEvent WHERE EventID = ?", [eventID]);
        window.location = "index.html";
    }, function(err) {
        console.log("DELETE EVENT ERROR " + err.code);
    }, function() {
        console.log("DELETE EVENT OK");
    });
}

function editEvent(ID, Name, LocationTitle, LocationLat, LocationLng, Date, Time, Organizer) {
    db.transaction(function(tx) {
        tx.executeSql("UPDATE Event SET Name = ?, Date = ?, Time = ?, Organizer = ? WHERE ID = ?",
            [Name, Date, Time, Organizer, ID]);
        tx.executeSql("UPDATE LocationEvent SET LocationTitle = ?, LocationLat=?, LocationLng=? WHERE EventID = ?",
            [LocationTitle, LocationLat, LocationLng, ID]);
        window.location = "detail.html?id=" + ID;
    }, function(err) {
        console.log("EDIT EVENT ERROR " + err.code);
    }, function() {
        onSuccessful();
    });
}