var db = null;

function connectDB() {
    db = window.openDatabase("MadDiscovery", 1.0, "Mad Discovery", 2000000);
    var url = window.location.href;
    if (db != null) {
        createTable();
        console.log("CONNECT DATABASE OK");
    }
}

function createTable() {
    db.transaction(function (tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS Event(ID INTEGER PRIMARY KEY AUTOINCREMENT, EventName, LocationTitle, LocationLat, LocationLng, Date, Time, Organizer);");
        tx.executeSql("CREATE TABLE IF NOT EXISTS Report(ID INTEGER PRIMARY KEY AUTOINCREMENT, EventID, Discription);");
        // tx.executeSql("CREATE TABLE IF NOT EXISTS LocationEvent(ID INTEGER PRIMARY KEY AUTOINCREMENT, EventID, LocationTitle, LocationLat, LocationLng);");
        tx.executeSql("CREATE TABLE IF NOT EXISTS LocationLive(ID INTEGER PRIMARY KEY AUTOINCREMENT, LocationTitle, LocationLat, LocationLng);");
        tx.executeSql("CREATE TABLE IF NOT EXISTS Image(ID INTEGER PRIMARY KEY AUTOINCREMENT, ImageURL, ReportID);");
    }, function (err) {
        console.log("CREATE TABLE ERROR " + err.message);
    }, function () {
        console.log("CREATE TABLE OK");
    });
}

function insertEvent(Name, LocationTitle, LocationLat, LocationLng, Date, Time, Organizer) {
    db.transaction(function (tx) {
        tx.executeSql("INSERT INTO Event(Name, LocationTitle, LocationLat, LocationLng, Date, Time, Organizer) VALUES(?, ?, ?, ?, ?, ?, ?);",
                [Name, LocationTitle, LocationLat, LocationLng, Date, Time, Organizer]);
        window.location = "index.html";
    }, function (err) {
        console.log("INSERT EVENT ERROR " + err.message);
    }, function () {
        console.log("INSERT EVENT Ok ");
    });
}

function insertReport(EventID, Discription) {
    db.transaction(function (tx) {
        tx.executeSql("INSERT INTO Report(EventID,Discription) VALUES(?,?);", [EventID, Discription]);
    }, function (err) {
        console.log("INSERT REPORT ERROR " + err.message);
    }, function () {
        console.log("INSERT REPORT OK");
    });
}

// Create new location you live now
function insertLocationLive(LocationTitle, LocationLat, LocationLng) {
    db.transaction(function (tx) {
        tx.executeSql("INSERT INTO LocationEvent(LocationTitle, LocationLat, LocationLng) VALUES(?,?,?);", [LocationTitle, LocationLat, LocationLng]);
    }, function (err) {
        console.log("INSERT LOCATION LIVE ERROR " + err.message);
    }, function () {
        console.log("INSERT LOCATION LIVE OK");
    });
}


function deleteEvent(eventID) {
    db.transaction(function (tx) {
        tx.executeSql("DELETE FROM Event WHERE ID = ?", [eventID]);
        tx.executeSql("DELETE FROM Report WHERE EventID = ?", [eventID]);
        tx.executeSql("DELETE FROM LocationEvent WHERE EventID = ?", [eventID]);
        window.location = "index.html";
    }, function (err) {
        console.log("DELETE EVENT ERROR " + err.code);
    }, function () {
        console.log("DELETE EVENT OK");
    });
}

function editEvent(ID, Name, LocationTitle, LocationLat, LocationLng, Date, Time, Organizer) {
    db.transaction(function (tx) {
        tx.executeSql("UPDATE Event SET Name = ?, Date = ?, Time = ?, Organizer = ? WHERE ID = ?",
                [Name, Date, Time, Organizer, ID]);
        tx.executeSql("UPDATE LocationEvent SET LocationTitle = ?, LocationLat=?, LocationLng=? WHERE EventID = ?",
                [LocationTitle, LocationLat, LocationLng, ID]);
        window.location = "detail.html?id=" + ID;
    }, function (err) {
        console.log("EDIT EVENT ERROR " + err.code);
    }, function () {
        onSuccessful();
    });
}